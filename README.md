<h1>Cypress Script</h1>

This repository contains code for testing a website "<B>Cypress.io</B>" by Radwa Ragab.

website link: https://www.ta3limy.com/

<B>» Main App objictive </B>

An educational platform that brings together parents, teachers and students to interact and develop each one according to their interests and career.

<B>» Pre-requisites </B>

- Node.js: https://nodejs.org/en/download/
- Visual Studio Code: https://code.visualstudio.com/download


<B>» Steps to use this project:</B>

1. Download or clone this repo
2. Install dependencies by running the following command in terminal (from inside your app directory i.e. where package.json is located): <I><B>npm install </I></B>



<B>» website licenses</B>

[MIT](https://choosealicense.com/licenses/mit/)


