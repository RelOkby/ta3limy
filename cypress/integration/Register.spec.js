
/// <reference types="cypress"/>

it ('Register as a student',()=> {
    
    cy.visit('https://www.ta3limy.com')
    cy.url().should('include','ta3limy')
        //Register
    cy.get('.e1a5eqzl0 > a').click()
        //As a student
    cy.get('#roles > :nth-child(2) > .css-ubxa2z').click()
        //Input Data
    cy.get('#firstName').type ("Rana").should('be.visible')
    cy.get('#lastName').type ("Ali").should('be.visible')
    cy.get(':nth-child(4) > .css-ubxa2z').type("01000012584")
        //Gender
    cy.get('#gender > :nth-child(2) > .css-ubxa2z').click()
        //DropDown Grade
    cy.xpath('//*[@id="grade"]')
    cy.xpath('/html/body/div[4]/div/section/div[2]/form/fieldset/div[4]/label').each(($el,index,$list)=>{
        var lan =$el.text()
        it(lan =='')
        {
            cy.wrap($el).click()
        }
    })
        //passwordAndConfirmation
    cy.get('#password').type("Study_Hard@22")
    cy.get('#passwordConfirmation').type("Study_Hard@22")
         //RadioButton of termsAndcondi  
         //cy.get('[type="radio"]').first().check()
    cy.get('button#googleRecaptcha > :nth-child(1)').should('be.disabled').check()
    //Recaptcha checkbox
    cy.xpath('/html/body/div[2]/div[3]/div[1]/div/div/span/div[1]').check()
    //sign in
    cy.get('.e11pp5v81').should('be.visible').click() 
})


