/// <reference types="cypress"/>

it ('Validating the common Questions and answers page contents',()=> {
    cy.visit('https://www.ta3limy.com')
    cy.get('#help-menu > svg').click()
    cy.xpath('/html/body/div[4]/div/header/div/div/div/ul/li[1]/a').click()
   
        //Ques1
    cy.get(':nth-child(1) > .Collapsible__trigger > .css-olkrgj').click()
        .get(':nth-child(1) > .Collapsible__contentOuter > .Collapsible__contentInner > p')
        .should('have.text' , 'مؤسسة ڤودافون مصر لتنمية المجتمع هي أول مؤسسة لا تهدف للربح في مجال الاتصالات في مصر وهي تابعة لوزارة التضامن الاجتماعي وتختلف عن قسم المسؤولية المجتمعية الخاص بالشركة. من وقت تأسيسها في سنة 2003 والمؤسسة بتحاول جاهدة إنها تنمي المجتمع من خلال إشراك منظمات المجتمع المدني والهيئات الغير حكومية في عملية تطوير المجتمع المصري في مجالات التعليم وتمكين ذوي الاحتياجات الخاصة.')
        //Ques2
    cy.get(':nth-child(2) > .Collapsible__trigger > .css-olkrgj').click()
        .get(':nth-child(2) > .Collapsible__contentOuter > .Collapsible__contentInner > p')
        .should('have.text','تعليمي هي منصة إلكترونية تعليمية مجانية للطلاب وأولياء الأمور والمعلمين') 
        //Ques3
    cy.get(':nth-child(3) > .Collapsible__trigger > .css-olkrgj').click()
        .get(':nth-child(3) > .Collapsible__contentOuter > .Collapsible__contentInner > p')
        .should('have.text','أولياء الأمور والطلاب في المرحلة الأولي وستستهدف المرحلة القادمة تقديم محتوى للمعلمين')
        //Ques4
    cy.get(':nth-child(4) > .Collapsible__trigger > .css-olkrgj').click()
        .get(':nth-child(4) > .Collapsible__contentOuter > .Collapsible__contentInner')
        .should('have.text','لأولياء الأمور يوجد:المعلومات عن التربية الإيجابية نصائح لتعليم الأولاد استخدام التكنولوجيا بطريقة آمنة خلال كتب إلكترونية باللغة العربية اختبار لاكتشاف قدرات الأولادك للطلاب يوجد:بالشراكة مع الأضواء يوجد مناهج لكل المراحل الدراسية ')
        //Ques5
    cy
    .get(':nth-child(5) > .Collapsible__trigger > .css-olkrgj').click()
        .get(':nth-child(5) > .Collapsible__contentOuter > .Collapsible__contentInner > p')
        .should('include','تعليمي مجاني لجميع الأفراد بشكل عام ويتميز عملاء ڤودافون بعدم السحب من باقة الانترنت بشكل خاص')
        //Ques6
    cy.get(':nth-child(6) > .Collapsible__trigger > .css-olkrgj').click()
        .get(':nth-child(6) > .Collapsible__contentOuter')
       // .should('have.text', ('في  ستصلك رسالة قصيرة مكونة من 6 أرقام ستظهر صفحة تملأ فيها الرمز وتؤكد أنك لست روبوتًا ثم اضغط علي إرسال ستتم إعادة توجيهك إلى ملف التعريف الخاص بكفي حال كان لديك حساب:ادخل على تعليمياضغط على زر تسجيل دخولاكتب رقم هاتفك وكلمة المرور كطالب ستجد لافتة كبيرة لـ الأضواء تضغط عليها ، وتتلقى رسالة تفيد بأنك ستتم إعادة توجيهك إلى الأضواء اضغط إتمام وابدأ رحلتك . كولي أمر، ستجد فئتين من أجلك ومن أجل طفلك يمكنك الضغط على الفئة التي تريدها وابدأ رحلتك ')
        //Ques7
    cy.get(':nth-child(7) > .Collapsible__trigger > .css-olkrgj').click()
        .get(':nth-child(7) > .Collapsible__contentOuter')
        .should('have.text','يتم تحديد الأشخاص الموجودين بلوحة المتصدرين على منصة تعليمي عن طريق الطرق التالية:')
 })
    








